var globalFunc = {
	init: function(){
		globalFunc.resize();
	},

	resize: function(){
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();

		// STICKY FOOTER
		var headerHeight = $('header').outerHeight();
		var footerHeight = $('footer').outerHeight();
		var footerTop = (footerHeight) * -1;
		$('footer').css({marginTop: footerTop});
		$('#main-wrapper').css({paddingBottom: footerHeight});

	},

	touch: function(){
		if (Modernizr.touch) {
			$('html').addClass('bp-touch');
		}
	},

	pwTab: function(){
		$('.kr-tab-nav .tab-nav a').click(function(){
			event.preventDefault();
			var _this = $(this).attr('data-id');
			$('.img-tab').hide();
			$('.'+_this).show();

			$('.kr-tab-nav .tab-nav a').removeClass('active');
			$(this).addClass('active');
		});
	},

	burgerMenu: function(){
		$('.burger-menu').click(function(){
			$(this).toggleClass('active');
			$('.menu-nav').toggleClass('active');
		});
	},

	animated: function(){
		if(!Modernizr.touch){
			$('.fadeUp').appear(function(){
				var elem = $(this);
				var el = elem.find('> *');
				setTimeout(function(){
					TweenMax.set($(elem), {opacity: 1});
					TweenMax.set($(el), {opacity: 0, y: 100});
					TweenMax.staggerTo($(el), 0.7, {opacity: 1, y: 0, delay: 0.3, ease: Quart.easeOut }, 0.3);
				}, 100);
			});

			$('.fadeLeft').appear(function(){
				var elem = $(this);
				var el = elem.find('> *');
				setTimeout(function(){
					TweenMax.set($(elem), {opacity: 1});
					TweenMax.set($(el), {opacity: 0, x: 100});
					TweenMax.staggerTo($(el), 0.7, {opacity: 1, x: 0, delay: 0.3, ease: Quart.easeOut }, 0.3);
				}, 100);
			});
			$('.fadeRight').appear(function(){
				var elem = $(this);
				var el = elem.find('> *');
				setTimeout(function(){
					TweenMax.set($(elem), {opacity: 1});
					TweenMax.set($(el), {opacity: 0, x: -100});
					TweenMax.staggerTo($(el), 0.7, {opacity: 1, x: 0, delay: 0.3, ease: Quart.easeOut }, 0.3);
				}, 100);
			});
			$('.fadeIn').appear(function(){
				var elem = $(this);
				var el = elem.find('> *');
				setTimeout(function(){
					TweenMax.set($(elem), {opacity: 1});
					TweenMax.set($(el), {opacity: 0});
					TweenMax.staggerTo($(el), 0.7, {opacity: 1, delay: 0.3, ease: Quart.easeOut }, 0.3);
				}, 100);
			});
		}
	},

	scrollNav: function(){
		$('.menu-nav a').each(function(){
			var _this = $(this);
			_this.click(function(){
				var element = _this.attr('href');
				$('html,body').animate({ scrollTop: $(element).offset().top}, 700);
				$('.menu-nav a').removeClass('active');
				_this.addClass('active');
				event.preventDefault();
			});
		});
	},

	vimeoVid: function(){
		var iframe = $('#vimeo_player')[0],
		player = $f(iframe),
		status = $('.status');
		player.addEvent('ready', function() {
			player.api('setVolume', 0);
		});
	}
};

$(window).resize(function() {
	globalFunc.init();
});

$(document).ready(function() {
	globalFunc.touch();
	globalFunc.init();
	globalFunc.pwTab();
	globalFunc.burgerMenu();
	globalFunc.animated();
	globalFunc.scrollNav();
	// globalFunc.vimeoVid();
});

$(window).on('load', function() {
	globalFunc.init();
});

// preloader once done
Pace.on('done', function() {
	// totally hide the preloader especially for IE
	setTimeout(function() {
		$('.pace-inactive').hide();
	}, 500);
});
